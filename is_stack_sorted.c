#include "push_swap.h"

int is_stack_sorted(t_stack *stack)
{
	if (!stack)
		return (0);
	if (!stack->next)
		return (1);
	while (stack->next)
	{
		if (stack->value > stack->next->value)
			return (0);
		stack = stack->next;
	}
	return (1);
}
