#include "push_swap.h"

int count_elements(t_stack *stack)
{
	int i;

	i = 0;
	while (stack)
	{
		stack = stack->next;
		i++;
	}
	return (i);
}

int count_operations(t_actions *op)
{
	int i;

	i = 0;
	while (op)
	{
		op = op->next;
		i++;
	}
	return (i);
}
