#include "push_swap.h"

void sorting_stack_a(t_stack **stack_a, t_stack **stack_b, int count, t_actions **op)
{
	int push_count;
	int median;
	int size;
	int rotate_count;

	push_count = 0;
	size = count;
	rotate_count = 0;
	if (count <= 3)
		return (sorting_three_or_less_on_top(stack_a, count, op));
	median = get_median(*stack_a, count);
	while (count >= 0)
	{
		if (ft_is_need_sort(*stack_a, median, count))
		{
			if ((*stack_a)->value < median && ++push_count)
				push_b_and_add_operation(stack_a, stack_b, op);//push_b(stack_a, stack_b);
			else
			{
				rotate_a_and_add_operation(stack_a, op);//rotate_a(stack_a);
				rotate_count++;
			}
		}
		count--;
	}
	while (rotate_count-- && count_elements(*stack_a) != size - push_count)
		reverse_rotate_a_and_add_operation(stack_a, op);//reverse_rotate_a(stack_a);
	sorting_stack_a(stack_a, stack_b, size - push_count, op);
	sorting_stack_b(stack_a, stack_b, push_count, op);
	while (push_count--)
		push_a_and_add_operation(stack_a, stack_b, op);//push_a(stack_a, stack_b);
}

void sorting_stack_b(t_stack **stack_a, t_stack **stack_b, int count, t_actions **op)
{
	int median;
	int push_count;
	int rotate_count;
	int size;

	push_count = 0;
	size = count;
	rotate_count = 0;
	if (count <= 3)
		return (rev_sorting_three_or_less_on_top(stack_b, count, op));
	median = get_median(*stack_b, count);
	while (count >= 0)
	{
		if (is_need_rev_sort(*stack_b, median, count))
		{
			if ((*stack_b)->value > median && ++push_count)
				push_a_and_add_operation(stack_a, stack_b, op);//push_a(stack_a, stack_b);
			else
			{
				rotate_b_and_add_operation(stack_b, op);//rotate_b(stack_b);
				rotate_count++;
			}
		}
		count--;
	}
	sorting_stack_a(stack_a, stack_b, push_count, op);
	while (rotate_count-- && count_elements(*stack_b) != size - push_count)
		reverse_rotate_b_and_add_operation(stack_b, op);//reverse_rotate_b(stack_b);
	sorting_stack_b(stack_a, stack_b, size - push_count, op);
	while (push_count--)
		push_b_and_add_operation(stack_a, stack_b, op);//push_b(stack_a, stack_b);
}
