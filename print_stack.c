#include "push_swap.h"

void print_stack(t_stack *stack)
{
	while (stack)
	{
		ft_putnbr(stack->value);
		ft_putstr(" ");
		stack = stack->next;
	}
	ft_putstr("\n");
}

void print_operations(t_actions *op)
{
	while (op)
	{
		ft_putstr(op->action);
		ft_putstr("\n");
		op = op->next;
	}
}

void ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
		write(1, &str[i++], 1);
}

void ft_putnbr(int number)
{
	unsigned int unum;
	char c;

	unum = number > 0 ? number : -number;
	if (number < 0)
		write(1, "-", 1);
	if (unum >= 10)
	{
		ft_putnbr(unum / 10);
		ft_putnbr(unum % 10);
	}
	else
	{
		c = unum + 48;
		write(1, &c, 1);
	}
}
