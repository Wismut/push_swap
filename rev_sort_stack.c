#include "push_swap.h"

void rev_sorting_three(t_stack **stack, t_actions **op)
{
	int prev_value;
	int cur_value;
	int next_value;

	prev_value = (*stack)->value;
	cur_value = (*stack)->next->value;
	next_value = (*stack)->next->next->value;
	if (prev_value > next_value && cur_value < next_value)
		rev_sorting_max_min_avg(stack, op);
	else if (cur_value < next_value && cur_value > prev_value)
		rev_sorting_min_avg_max(stack, op);
	else if (prev_value < cur_value && prev_value > next_value)
		rev_sorting_avg_max_min(stack, op);
	else if (prev_value > cur_value && prev_value < next_value)
		rev_sorting_avg_min_max(stack, op);
	else if (next_value > prev_value && next_value < cur_value)
		rev_sorting_min_max_avg(stack, op);
}

void rev_sorting_three_or_less(t_stack **stack, int len, t_actions **op)
{
	int temp;

	if (len == 1)
		return;
	else if (len == 2)
	{
		if ((*stack)->value < (*stack)->next->value)
		{
			swap_b_and_add_operation(stack, op);
			//temp = (*stack)->value;
			//(*stack)->value = (*stack)->next->value;
			//(*stack)->next->value = temp;
		}
	}
	else
		rev_sorting_three(stack, op);
}

void rev_sorting_three_on_top(t_stack **stack, t_actions **op)
{
	int prev_value;
	int cur_value;
	int next_value;

	prev_value = (*stack)->value;
	cur_value = (*stack)->next->value;
	next_value = (*stack)->next->next->value;
	if (prev_value < cur_value && cur_value < next_value)
		rev_sort_min_avg_max_on_top(stack, op);
	else if (prev_value < cur_value && prev_value > next_value)
		rev_sorting_avg_max_min_on_top(stack, op);
	else if (prev_value > cur_value && prev_value < next_value)
		rev_sorting_avg_min_max_on_top(stack, op);
	else if (next_value < prev_value && cur_value < next_value)
		rev_sorting_max_min_avg_on_top(stack, op);
	else if (next_value > prev_value && next_value < cur_value)
		rev_sorting_min_max_avg_on_top(stack, op);
}

void rev_sorting_three_or_less_on_top(t_stack **stack, int number, t_actions **op)
{
	if (count_elements(*stack) <= 3)
		return (rev_sorting_three_or_less(stack, number, op));
	if (number == 1)
		return;
	else if (number == 2)
	{
		if ((*stack)->value < (*stack)->next->value)
			swap_b_and_add_operation(stack, op);
	}
	else
		rev_sorting_three_on_top(stack, op);
}
