#include "push_swap.h"

void sorting_max_avg_min_on_top(t_stack **stack, t_actions **op)
{
	swap_a_and_add_operation(stack, op);//swap_a(stack);
	rotate_a_and_add_operation(stack, op);//rotate_a(stack);
	swap_a_and_add_operation(stack, op);//swap_a(stack);
	reverse_rotate_a_and_add_operation(stack, op);//reverse_rotate_a(stack);
	swap_a_and_add_operation(stack, op);//swap_a(stack);
}

void sorting_max_min_avg_on_top(t_stack **stack, t_actions **op)
{
	swap_a_and_add_operation(stack, op);//swap_a(stack);
	rotate_a_and_add_operation(stack, op);//rotate_a(stack);
	swap_a_and_add_operation(stack, op);//swap_a(stack);
	reverse_rotate_a_and_add_operation(stack, op);//reverse_rotate_a(stack);
}

void sorting_avg_max_min_on_top(t_stack **stack, t_actions **op)
{
	rotate_a_and_add_operation(stack, op);//rotate_a(stack);
	swap_a_and_add_operation(stack, op);//swap_a(stack);
	reverse_rotate_a_and_add_operation(stack, op);//reverse_rotate_a(stack);
	swap_a_and_add_operation(stack, op);//swap_a(stack);
}

void sorting_avg_min_max_on_top(t_stack **stack, t_actions **op)
{
	swap_a_and_add_operation(stack, op);//swap_a(stack);
}

void sorting_min_max_avg_on_top(t_stack **stack, t_actions **op)
{
	rotate_a_and_add_operation(stack, op);//rotate_a(stack);
	swap_a_and_add_operation(stack, op);//swap_a(stack);
	reverse_rotate_a_and_add_operation(stack, op);//reverse_rotate_a(stack);
}
