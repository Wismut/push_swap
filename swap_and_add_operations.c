#include "push_swap.h"

void swap_a_and_add_operation(t_stack **stack_a, t_actions **op)
{
	swap_a(stack_a);
	*op = add_operation(*op, "sa");
}

void swap_b_and_add_operation(t_stack **stack_b, t_actions **op)
{
	swap_b(stack_b);
	*op = add_operation(*op, "sb");
}

void swap_a_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op)
{
	swap_a_b(stack_a, stack_b);
	*op = add_operation(*op, "ss");
}
