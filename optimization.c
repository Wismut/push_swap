#include "push_swap.h"

void del_two_elements(t_actions **action)
{
	t_actions *to_del1;
	t_actions *to_del2;

	to_del1 = (*action)->next;
	to_del2 = (*action)->next->next;
	(*action)->next = (*action)->next->next->next;
	free(to_del2);
	free(to_del1);
}

int optimization_del(t_actions **op)
{
	t_actions *cur;
	t_actions *head;
	int len;

	head = *op;
	len = count_operations(*op);
	while (*op)
	{
		cur = (*op)->next;
		if (!cur || !cur->next)
			break;
		if ((!ft_strcmp(cur->action, "ra") && !ft_strcmp(cur->next->action, "rra")) ||
			(!ft_strcmp(cur->action, "rb") && !ft_strcmp(cur->next->action, "rrb")) ||
			(!ft_strcmp(cur->action, "rra") && !ft_strcmp(cur->next->action, "ra")) ||
			(!ft_strcmp(cur->action, "rrb") && !ft_strcmp(cur->next->action, "rb")) ||
			(!ft_strcmp(cur->action, "sa") && !ft_strcmp(cur->next->action, "sa")) ||
			(!ft_strcmp(cur->action, "sb") && !ft_strcmp(cur->next->action, "sb")) ||
			(!ft_strcmp(cur->action, "pa") && !ft_strcmp(cur->next->action, "pb")) ||
			(!ft_strcmp(cur->action, "pb") && !ft_strcmp(cur->next->action, "pa")))
			del_two_elements(op);
		*op = (*op)->next;
	}
	*op = head;
	return (len - count_operations(*op));
}

void merge_two_elements(t_actions **action, char *str)
{
	t_actions *to_del;

	to_del = (*action)->next;
	(*action)->next = (*action)->next->next;
	(*action)->next->action = str;
	free(to_del);
}

int optimization_merge(t_actions **op)
{
	t_actions *cur;
	t_actions *head;
	int len;

	head = *op;
	len = count_operations(*op);
	while (*op && (*op)->next && (*op)->next->next)
	{
		cur = (*op)->next;
		if ((!ft_strcmp(cur->action, "sa") && !ft_strcmp(cur->next->action, "sb")) ||
		(!ft_strcmp(cur->action, "sb") && (!ft_strcmp(cur->next->action, "sa"))))
			merge_two_elements(op, "ss");
		else if ((!ft_strcmp(cur->action, "ra") && (!ft_strcmp(cur->next->action, "rb"))) ||
		(!ft_strcmp(cur->action, "rb") && (!ft_strcmp(cur->next->action, "ra"))))
			merge_two_elements(op, "rr");
		else if ((!ft_strcmp(cur->action, "rra") && (!ft_strcmp(cur->next->action,
"rrb"))) || (!ft_strcmp(cur->action, "rrb") && (!ft_strcmp(cur->next->action, "rra"))))
			merge_two_elements(op, "rrr");
		*op = (*op)->next;
	}
	*op = head;
	return (len - count_operations(*op));
}

void two_to_three(t_actions **action, char *s1, char *s2)
{
	t_actions *to_del;

	to_del = (*action)->next;
	(*action)->next = (*action)->next->next;
	(*action)->next->action = s1;
	(*action)->next->next->action = s2;
	free(to_del);
}

int optimization_two_to_three(t_actions **action)
{
	t_actions *cur;
	t_actions *head;
	int len;

	head = *action;
	len = count_operations(*action);
	while (*action)
	{
		cur = (*action)->next;
		if (!cur || !cur->next)
			break;
		if (!ft_strcmp(cur->action, "ra") && !ft_strcmp(cur->next->action, "pb") &&
			!ft_strcmp(cur->next->next->action, "rra"))
			two_to_three(action, "sa", "pb");
		else if (!ft_strcmp(cur->action, "rb") && !ft_strcmp(cur->next->action, "pa") &&
				 !ft_strcmp(cur->next->next->action, "rrb"))
			two_to_three(action, "sb", "pa");
		*action = (*action)->next;
	}
	*action = head;
	return (len - count_operations(*action));
}
