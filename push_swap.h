#ifndef PUSH_SWAP_PUSH_SWAP_H
#define PUSH_SWAP_PUSH_SWAP_H

# include "string.h"
# include "unistd.h"
# include "stdlib.h"

typedef struct s_stack
{
	int value;
	struct s_stack *next;
} t_stack;

typedef struct s_actions
{
	char *action;
	struct s_actions *next;
} t_actions;

int count_elements(t_stack *stack);

void reverse_rotate_a(t_stack **stack_a);

void reverse_rotate_b(t_stack **stack_b);

void reverse_rotate_a_b(t_stack **stack_a, t_stack **stack_b);

void rotate_a(t_stack **stack_a);

void rotate_b(t_stack **stack_b);

void rotate_a_b(t_stack **stack_a, t_stack **stack_b);

void push_a(t_stack **stack_a, t_stack **stack_b);

void push_b(t_stack **stack_a, t_stack **stack_b);

void swap_a(t_stack **stack_a);

void swap_b(t_stack **stack_b);

void swap_a_b(t_stack **stack_a, t_stack **stack_b);

int get_median(t_stack *stack, int count);

void recursion(t_stack **stack_a, t_stack **stack_b, int count);

void sorting_max_avg_min(t_stack **stack, t_actions **op);

void sorting_max_min_avg(t_stack **stack, t_actions **op);

void sorting_avg_max_min(t_stack **stack, t_actions **op);

void sorting_avg_min_max(t_stack **stack, t_actions **op);

void sorting_min_max_avg(t_stack **stack, t_actions **op);

void print_stack(t_stack *stack);

int is_stack_sorted(t_stack *stack);

void sorting_three_or_less(t_stack **stack, int len, t_actions **op);

int ft_is_need_sort(t_stack *stack, int median, int count);

t_actions *add_operation(t_actions *head, char *op);

void sorting_stack_a(t_stack **stack_a, t_stack **stack_b, int count, t_actions **op);

void sorting_stack_b(t_stack **stack_a, t_stack **stack_b, int count, t_actions **op);

void sorting_three_or_less(t_stack **stack, int len, t_actions **op);

void sorting_max_avg_min_on_top(t_stack **stack, t_actions **op);

void sorting_max_min_avg_on_top(t_stack **stack, t_actions **op);

void sorting_avg_max_min_on_top(t_stack **stack, t_actions **op);

void sorting_avg_min_max_on_top(t_stack **stack, t_actions **op);

void sorting_min_max_avg_on_top(t_stack **stack, t_actions **op);

void rev_sorting_max_min_avg_on_top(t_stack **stack, t_actions **op);

void rev_sorting_avg_max_min_on_top(t_stack **stack, t_actions **op);

void rev_sorting_avg_min_max_on_top(t_stack **stack, t_actions **op);

void rev_sorting_min_max_avg_on_top(t_stack **stack, t_actions **op);

void rev_sorting_max_min_avg(t_stack **stack, t_actions **op);

void rev_sorting_avg_max_min(t_stack **stack, t_actions **op);

void rev_sorting_avg_min_max(t_stack **stack, t_actions **op);

void rev_sorting_min_max_avg(t_stack **stack, t_actions **op);

void rev_sorting_min_avg_max(t_stack **stack, t_actions **op);

void rev_sorting_three_or_less(t_stack **stack, int len, t_actions **op);

void rev_sorting_three_on_top(t_stack **stack, t_actions **op);

void rev_sorting_three_or_less_on_top(t_stack **stack, int number, t_actions **op);

void ft_putstr(char *str);

void ft_putnbr(int number);

void print_operations(t_actions *op);

void rev_sort_min_avg_max_on_top(t_stack **stack, t_actions **op);

void sorting_three_or_less_on_top(t_stack **stack, int number, t_actions **op);

int is_need_rev_sort(t_stack *stack, int median, int count);

void push_a_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op);

void push_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op);

void reverse_rotate_b_and_add_operation(t_stack **stack_b, t_actions **op);

void reverse_rotate_a_and_add_operation(t_stack **stack_a, t_actions **op);

void reverse_rotate_a_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op);

void rotate_a_and_add_operation(t_stack **stack_a, t_actions **op);

void rotate_b_and_add_operation(t_stack **stack_b, t_actions **op);

void rotate_a_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op);

void swap_a_and_add_operation(t_stack **stack_a, t_actions **op);

void swap_b_and_add_operation(t_stack **stack_b, t_actions **op);

void swap_a_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op);

size_t ft_strlen(const char *s);

void ft_error(char *error);

void is_valid(char *string);

void cheak_repeat(t_stack *stack, int number);

int ft_strcmp(const char *s1, const char *s2);

int count_operations(t_actions *op);

int optimization_del(t_actions **op);

int optimization_merge(t_actions **op);

int optimization_two_to_three(t_actions **action);

#endif
