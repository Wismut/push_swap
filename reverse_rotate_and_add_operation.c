#include "push_swap.h"

void reverse_rotate_a_and_add_operation(t_stack **stack_a, t_actions **op)
{
	reverse_rotate_a(stack_a);
	*op = add_operation(*op, "rra");
}

void reverse_rotate_b_and_add_operation(t_stack **stack_b, t_actions **op)
{
	reverse_rotate_b(stack_b);
	*op = add_operation(*op, "rrb");
}

void reverse_rotate_a_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op)
{
	reverse_rotate_a_b(stack_a, stack_b);
	*op = add_operation(*op, "rrr");
}
