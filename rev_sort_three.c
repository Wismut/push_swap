#include "push_swap.h"

void rev_sorting_max_min_avg(t_stack **stack, t_actions **op)
{
	swap_b_and_add_operation(stack, op);
	rotate_b_and_add_operation(stack, op);
	//swap_b(stack);
	//rotate_b(stack);
}

void rev_sorting_avg_max_min(t_stack **stack, t_actions **op)
{
	swap_b_and_add_operation(stack, op);
	//swap_b(stack);
}

void rev_sorting_avg_min_max(t_stack **stack, t_actions **op)
{
	reverse_rotate_b_and_add_operation(stack, op);
	//reverse_rotate_b(stack);
}

void rev_sorting_min_max_avg(t_stack **stack, t_actions **op)
{
	rotate_b_and_add_operation(stack, op);
	//rotate_b(stack);
}

void rev_sorting_min_avg_max(t_stack **stack, t_actions **op)
{
	swap_b_and_add_operation(stack, op);
	reverse_rotate_b_and_add_operation(stack, op);
	//swap_b(stack);
	//reverse_rotate_b(stack);
}
