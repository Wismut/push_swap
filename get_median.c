#include "push_swap.h"

int count_more_value(t_stack *stack, int value, int len)
{
	int i;

	i = 0;
	while (stack && len--)
	{
		if (stack->value > value)
			i++;
		stack = stack->next;
	}
	return (i);
}

int count_less_value(t_stack *stack, int value, int len)
{
	int i;

	i = 0;
	while (stack && len--)
	{
		if (stack->value < value)
			i++;
		stack = stack->next;
	}
	return (i);
}

int get_median(t_stack *stack, int count)
{
	int len;
	t_stack *head;

	head = stack;
	len = count;
	while (stack && count--)
	{
		if (count_more_value(head, stack->value, len) == count_less_value(head, stack->value, len) ||
				count_more_value(head, stack->value, len) == count_less_value(head, stack->value, len) - 1)
			return (stack->value);
		stack = stack->next;
	}
	return (0);
}
