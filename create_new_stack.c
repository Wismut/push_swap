#include "push_swap.h"

t_actions *add_operation(t_actions *head, char *op)
{
	t_actions *op_head;
	static int i = 0;

	i--;
	op_head = head;
	if (!head)
	{
		head = (t_actions *) malloc(sizeof(t_actions));
		head->action = op;
		head->next = NULL;
		return (head);
	}
	while (head->next)
		head = head->next;
	head->next = (t_actions *) malloc(sizeof(t_actions));
	head = head->next;
	head->action = op;
	head->next = NULL;
	return (op_head);
}
