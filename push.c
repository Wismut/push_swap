#include "push_swap.h"

void push_a(t_stack **stack_a, t_stack **stack_b)
{
	t_stack *head_a;

	if (!stack_b || !*stack_b)
		return;
	head_a = *stack_b;
	*stack_b = (*stack_b)->next;
	head_a->next = *stack_a;
	*stack_a = head_a;
}

void push_b(t_stack **stack_a, t_stack **stack_b)
{
	t_stack *head_b;

	if (!stack_a || !*stack_a)
		return;
	head_b = *stack_a;
	*stack_a = (*stack_a)->next;
	head_b->next = *stack_b;
	*stack_b = head_b;
}
