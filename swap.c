#include "push_swap.h"

void swap_a(t_stack **stack_a)
{
	int temp;

	if (count_elements(*stack_a) <= 1)
		return;
	temp = (*stack_a)->value;
	(*stack_a)->value = (*stack_a)->next->value;
	(*stack_a)->next->value = temp;
}

void swap_b(t_stack **stack_b)
{
	int temp;

	if (count_elements(*stack_b) <= 1)
		return;
	temp = (*stack_b)->value;
	(*stack_b)->value = (*stack_b)->next->value;
	(*stack_b)->next->value = temp;
}

void swap_a_b(t_stack **stack_a, t_stack **stack_b)
{
	swap_a(stack_a);
	swap_b(stack_b);
}
