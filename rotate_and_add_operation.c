#include "push_swap.h"

void rotate_a_and_add_operation(t_stack **stack_a, t_actions **op)
{
	rotate_a(stack_a);
	*op = add_operation(*op, "ra");
}

void rotate_b_and_add_operation(t_stack **stack_b, t_actions **op)
{
	rotate_b(stack_b);
	*op = add_operation(*op, "rb");
}

void rotate_a_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op)
{
	rotate_a_b(stack_a, stack_b);
	*op = add_operation(*op, "rr");
}
