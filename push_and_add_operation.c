#include "push_swap.h"

void push_a_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op)
{
	push_a(stack_a, stack_b);
	*op = add_operation(*op, "pa");
}

void push_b_and_add_operation(t_stack **stack_a, t_stack **stack_b, t_actions **op)
{
	push_b(stack_a, stack_b);
	*op = add_operation(*op, "pb");
}
