#include "push_swap.h"

void ft_error(char *error)
{
	ft_putstr(error);
	exit(0);
}

void is_valid(char *string)
{
	int i;

	i = 0;
	if (!string)
		ft_error("Error\n");
	if (ft_strlen(string) > 11)
		ft_error("Error\n");
	while (string[i])
	{
		if (((string[i] == '+' || string[i] == '-') && i != 0) ||
			(string[i] < '0' && string[i] != '-' && string[i] != '+') || string[i] > '9')
			ft_error("Error\n");
		i++;
	}
}

long ft_atol(char *str)
{
	unsigned long res;
	int i;
	int isneg;

	res = 0;
	i = 0;
	isneg = 0;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
		i++;
	if ((str[i] == '-' || str[i] == '+')
		&& (str[i + 1] > '9' || str[i + 1] < '0'))
		return (0);
	if (str[i] == '-')
		isneg = -1;
	if (str[i] == '-' || str[i] == '+')
		i++;
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			break;
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (isneg < 0 ? -res : res);
}

int main(int argc, char **argv)
{
	t_stack *a;
	t_stack *b;
	t_stack *head_a;
	t_actions *ops_head;
	int count;
	long value;

	count = 0;
	b = NULL;
	a = (t_stack *) malloc(sizeof(a));
	head_a = a;
	ops_head = NULL;
	if (argc == 1)
		ft_error("Usage\nPlease insert values between -2147483648 and 2147483647\n");
	while (++count < argc)
	{
		is_valid(argv[count]);
		value = ft_atol(argv[count]);
		value < -2147483648 || value > 2147483647 ? ft_error("Error\n") : 0;
		a->value = value;
		a->next = NULL;
		cheak_repeat(head_a, value);
		count + 1 != argc ? a->next = (t_stack *) malloc(sizeof(a)) : 0;
		count + 1 != argc ? a = a->next : 0;
	}
	a->next = NULL;
	sorting_stack_a(&head_a, &b, count_elements(head_a), &ops_head);
	while (optimization_del(&ops_head));
	while (optimization_merge(&ops_head));
	while (optimization_two_to_three(&ops_head));
	print_operations(ops_head);
}
