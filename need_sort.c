#include "push_swap.h"

int ft_is_need_sort(t_stack *stack, int median, int count)
{
	while (count && stack)
	{
		--count;
		if (stack->value <= median)
			return (1);
		stack = stack->next;
	}
	return (0);
}

int is_need_rev_sort(t_stack *stack, int median, int count)
{
	while (count && stack)
	{
		--count;
		if (stack->value >= median)
			return (1);
		stack = stack->next;
	}
	return (0);
}
