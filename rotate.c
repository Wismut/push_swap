#include "push_swap.h"

void rotate_a(t_stack **stack_a)
{
	t_stack *head;
	t_stack *last;

	if (count_elements(*stack_a) <= 1)
		return;
	head = *stack_a;
	last = *stack_a;
	while (last->next)
		last = last->next;
	last->next = head;
	*stack_a = head->next;
	head->next = NULL;
}

void rotate_b(t_stack **stack_b)
{
	t_stack *head;
	t_stack *last;

	if (count_elements(*stack_b) <= 1)
		return;
	head = *stack_b;
	last = *stack_b;
	while (last->next)
		last = last->next;
	last->next = head;
	*stack_b = head->next;
	head->next = NULL;
}

void rotate_a_b(t_stack **stack_a, t_stack **stack_b)
{
	rotate_a(stack_a);
	rotate_b(stack_b);
}
